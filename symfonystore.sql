-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28.05.2017 klo 12:35
-- Palvelimen versio: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `symfonystore`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Vedos taulusta `category`
--

INSERT INTO `category` (`id`, `name`, `urlname`) VALUES
(1, 'Elektroniikka', 'elektroniikka'),
(2, 'Kodinkoneet', 'kodinkoneet'),
(3, 'Urheilu', 'urheilu'),
(4, 'Koti', 'koti'),
(5, 'Remontointi', 'remontointi');

-- --------------------------------------------------------

--
-- Rakenne taululle `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `imgurl1` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `imgurl2` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `imgurl3` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD12469DE2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Vedos taulusta `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `description`, `category_id`, `imgurl1`, `imgurl2`, `imgurl3`, `details`) VALUES
(1, 'Huawei Honor 8', '219.90', 'Honor 8 Lite on näyttävä puhelin premium-luokan designilla ja viimeistelyllä. Honor 8 Lite ei kuitenkaan ole vain kaunis kuori, sillä tehokas sisin ja helppokäyttöinen käyttöliittymä luovat vertaansa vailla olevan käyttökokemuksen. Honor 8 Lite on yhdistelmä timanttista laatua rohkeaan hintaan.\r\n\r\n5,2" Full HD näyttö\r\n12 MP takakamera\r\n16 GB tallennustila\r\n8-ydinprosessori ja 3GB RAM\r\nDual-SIM ominaisuus\r\nWiFi, Bluetooth 4.1\r\n3000 mAh akku\r\nAndroid 7.0 Nougat ', 1, 'https://www.power.fi/images/products/6901443160938_HUAWEI_HONOR8LITEWH_F.jpg?height=250&width=250&bgcolor=fff&format=jpg&quality=100', 'https://www.power.fi/images/products/6901443160938_HUAWEI_HONOR8LITEWH_2.jpg?height=450&width=450&bgcolor=fff', 'https://www.power.fi/images/products/6901443160938_HUAWEI_HONOR8LITEWH_3.jpg?height=450&width=450&bgcolor=fff', ''),
(2, 'Samsung Galaxy S8 Orchid Grey', '829.00', 'Ympäröivä infinity-näyttö linebreak Galaxy S8 muuttaa käsityksesi puhelimen käyttökokemuksesta ja muotoilusta. Infinity-näytön kehykset ja reunat ovat hillitympiä, mikä luo rajattoman vaikutelman. Painikkeet on integroitu tyylikkäästi näytön käyttöliittymään ja ne mukautuvat kulloiseenkin näyttötilaan. Fyysinen kotipainike on jäljellä paineanturin muodossa. linebreak Rajattomia elämyksiä linebreak Ohuen näytön ansiosta kirjoittaminen ja navigointi käyvät helposti yhdelläkin kädellä ja vähemmällä selaamisella. Videoiden ja kuvien katseluelämys vahvistuu ja sovelluksesta toiseen voi siirtyä ilman viiveitä. Galaxy S8- ja Galaxy S8+ -malleista on tarjolla kolme eri väriä. linebreak Tarkkoja kuvia joka tilanteessa linebreak Jokaisessa kameran pikselissä on kaksi erillistä valodiodia, jotka toimivat yhdessä silmäparin tapaan. Siksi automaattitarkennus on nopeaa ja tarkkaa. Suurempi valovoimaisuus mahdollistaa myös kuvaamisen hämärissä olosuhteissa. linebreak Suojaa ja helpota digitaalista elämääsi linebreak Iiris- ja sormenjälkitunnistus tekevät mobiilista elämästäsi turvallista ja sujuvaa. Voit suojata digitaalista elämääsi silmillä tai sormenjäljellä – voit valita mielestäsi mukavimman vaihtoehdon. Iiristunnistus on jopa 200 kertaa varmempi kuin sormenjälkitunnistus. linebreak Kestää sääoloja linebreak Veden ja pölyn ei kannata antaa rajoittaa puhelimen käyttöä. Galaxy S8 selviää IP68-luokituksen mukaisesti 1,5 metrin syvyydessä 30 minuuttia. linebreak Tässä on Bixby linebreak Bixby muuttaa täysin tavan, jolla olet vuorovaikutuksessa puhelimesi kanssa. Se on henkilökohtainen assistentti, joka ymmärtää puhetta, tekstiä ja napautuksia, joten viestintätapoja voi vaihdella tarpeen mukaan.* Se muuttaa täysin myös tavan, jolla haet eri asioita. Sinun ei edes tarvitse kysyä – näytät vain Bixbylle mitä haluat avaamalla kamerasovelluksen. Se on todella helppoa. linebreak', 1, 'https://www.power.fi/images/products/8806088708805_SAMSUNG_GXYS8OG_F.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/8806088708805_SAMSUNG_GXYS8OG_2.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/8806088708805_SAMSUNG_GXYS8OG_3.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'Valmistaja: Samsung,\r\nVirta ja akku: Akun kapasiteetti: 3000 mAh,\r\nVirta ja akku: Integroitu langaton lataus: Kyllä,\r\nVirta ja akku: Latausjohdon tyyppi: USB C,\r\nVirta ja akku: Vaihdettava akku: Ei,\r\nIskunkestävä: Ei,\r\nSormenjälkitunnistin: Kyllä,\r\nVettä hylkivä 	Kyllä,\r\nKäyttöjärjestelmä (perhe): Android,\r\nKäyttöjärjestelmä (versio): Android 7.0,\r\nPaneelin tyyppi: AMOLED,\r\nNäytön resoluutio: 2960x1440,\r\nNäytön koko: 5,8 tuumaa,\r\nNäytön teknologia: LED'),
(3, 'Apple iPhone 7 Plus 128Gb Silver', '999.90', 'Uusi iPhone 7 parantaa merkittävästi iPhonen käyttökokemuksen tärkeimpiä osa-alueita. linebreak Siinä on uudet edistykselliset kamerajärjestelmät. iPhonen kaikkien aikojen paras suorituskyky ja akunkesto. Muhkeaääniset stereokaiuttimet. Kirkkain ja värikkäin iPhone-näyttö. Se on roisketiivis.1 Ja siinä on komeasti tehoa. Se on iPhone 7.', 1, 'https://www.power.fi/images/products/190198043962_iphone7p_slv_F.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/iphone7p_slv_F_1.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://astro-chologist.com/wp-content/uploads/2013/08/blank-white.jpg', 'Näyttö: 5.5" Retina HD,\r\nPiiri: A10 Fusion,\r\nKamerat: 2x 12 Mpix,\r\nVedenpitävyysluokitus: IP67,\r\nSormenjälki­tunnistin: Touch ID,\r\nÄäni: Stereokaiuttimet,\r\nKäyttöjärjestelmä: iOS 10'),
(4, 'Samsung Galaxy A5', '398.90', 'Uusissa Galaxy A -sarjan puhelimissa on parempi kamera, terävämpi näyttö ja pidempi akkukesto. Voit käyttää puhelinta huoletta rannalla, kaatosateessa tai laskettelurinteessä. Galaxy A on sekä pölyn- että vedenkestävä IP68-luokituksen* mukaisesti.\r\n \r\nMateriaalit ovat yhtä korkealaatuista lasia ja metallia kuin Galaxy S7 -mallissa, joten puhelin luo ylellisen tuntuman. Saat kohtuuhintaisen puhelimen, joka tuntuu yhtä ylelliseltä kuin huippumallit. Valittavana on kaksi kokoa ja kolme väriä.\r\n\r\n5,2” sAMOLED näyttö\r\n16 MP takakamera\r\n16 MP etukamera\r\n32 GB tallennustila\r\n8-ydinsuoritin 1,9GHz\r\nIP68 -sertifioitu\r\nSormenjälkitunnistin\r\nAndroid 6.0 ', 1, 'https://www.power.fi/images/products/8806088602011_SAMSUNG_GXYA52017_2.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/8806088602011_SAMSUNG_GXYA52017_F.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/8806088602011_SAMSUNG_GXYA52017_3.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'Valmistaja: Samsung,\r\nVirta ja akku: Akun kapasiteetti: 3000 mAh,\r\nVirta ja akku: Integroitu langaton lataus: Kyllä,\r\nIskunkestävä: Ei,\r\nKamera: CMOS 16,0 MP,\r\nVettä hylkivä 	Kyllä,\r\nKäyttöjärjestelmä (perhe): Android,\r\nKäyttöjärjestelmä (versio): Android 7.0,\r\nPaneelin tyyppi: LED,\r\nNäytön koko: 5,2",\r\nNäytön teknologia: LED'),
(9, 'Samsung Galaxy A3', '298.90', 'Uusissa Galaxy A -sarjan puhelimissa on parempi kamera, terävämpi näyttö ja pidempi akkukesto. Voit käyttää puhelinta huoletta rannalla, kaatosateessa tai laskettelurinteessä. Galaxy A on sekä pölyn- että vedenkestävä IP68-luokituksen* mukaisesti.\r\n \r\nMateriaalit ovat yhtä korkealaatuista lasia ja metallia kuin Galaxy S7 -mallissa, joten puhelin luo ylellisen tuntuman. Saat kohtuuhintaisen puhelimen, joka tuntuu yhtä ylelliseltä kuin huippumallit. Valittavana on kaksi kokoa ja kolme väriä.\r\n\r\n4,7” sAMOLED näyttö\r\n13 MP takakamera\r\n8 MP etukamera\r\n16 GB tallennustila\r\n8-ydinsuoritin 1,6GHz\r\nIP68 -sertifioitu\r\nSormenjälkitunnistin\r\nAndroid 6.0 ', 1, 'https://www.power.fi/images/products/8806088632520_SAMSUNG_GXYA3P_3.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/8806088632520_SAMSUNG_GXYA3P_F.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/8806088632520_SAMSUNG_GXYA3P_2.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'Valmistaja: Samsung,\r\nVirta ja akku: Akun kapasiteetti: 3000 mAh,\r\nVirta ja akku: Integroitu langaton lataus: Kyllä,\r\nIskunkestävä: Ei,\r\nKamera: CMOS 16,0 MP,\r\nVettä hylkivä 	Kyllä,\r\nKäyttöjärjestelmä (perhe): Android,\r\nKäyttöjärjestelmä (versio): Android 7.0,\r\nPaneelin tyyppi: LED,\r\nNäytön koko: 5,2",\r\nNäytön teknologia: LED'),
(10, 'Doro 624', '169.80', 'Tyylikäs kamerapuhelin kuuluvalla ja selkeällä äänellä. Kaikki yksityiskohdat ja toiminnot on suunniteltu helppokäyttöisyyttä varten ja voit jopa lähettää videoviestejä yhtä vaivattomasti kuin voit ottaa ja jakaa valokuvia. Muita ominaisuuksia ovat pikavalinta- ja tekstiviestipainikkeet, pöytälaturi, Bluetooth®, sääennusteet ja etähallinta My Doro Manager -sovelluksen avulla.\r\n\r\nHelppokäyttöinen kamera\r\nVoimakas ja selkeä ääni\r\nSelkeä näppäimistö\r\n2 MP kamera\r\nHAC-kuulolaite yhteensopiva', 1, 'https://www.power.fi/images/products/7322460065324_624_1.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/7322460065324_Doro_PhoneEasy_624_red_white_closed_left.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/7322460065324_624_side_1.jpg?height=450&width=450&bgcolor=fff&format=jpg', ''),
(12, 'ZTE BLADE A310', '99.00', 'ZTE älypuhelin jossa 5" näyttö ja 8 MP kamera\r\n\r\n\r\n\r\n    5" näyttö\r\n    Dual-SIM ominaisuus\r\n    2200 mAh akku\r\n    8 MP kamera, 2 MP etukamera\r\n    Snapdragon 210 Quad-core 1,3 GHz prosessori\r\n    8 GB tallennustila\r\n    Android 6.0 ', 1, 'https://www.power.fi/images/products/6902176000737_Diverse_17849_EN_F.png?height=450&width=450&bgcolor=fff&format=jpg', 'https://astro-chologist.com/wp-content/uploads/2013/08/blank-white.jpg', 'https://astro-chologist.com/wp-content/uploads/2013/08/blank-white.jpg', ''),
(13, 'Samsung 23B-pesukone', '799.00', 'Tässä Samsung pesukoneessa on keraaminen vastus. linebreak \r\n\r\nTämän ansiosta pesukoneesi käyttöikä pitenee. Keraaminen pinnoite estää epäpuhtauksien tarttumisen lämpövastuksen pintaan. Keraamisen lämpövastuksen täydellinen lämmitysteho on jopa 10 vuoden päästä melkein sama kuin uutena. Verrattuna perinteiseen lämpövastukseen Samsungin keraaminen lämpövastus ylläpitää halutun pesulämpötilan tarkemmin ja tehokkaammin,kuin perinteiset rautavastukset. Tämän ansiosta lämmitysteho on paras mahdollinen. linebreak \r\n\r\nRummun puhdistus ilman kemikaaleja linebreak \r\n\r\nEco Drum Clean pitää pesurummun puhtaana ilman vahvoja kemikaaleja. Lisäksi se antaa ilmoituksen puhdistuksen tarpeesta. Rummunpesuohjelma puhdistaa tehokkaasti koko rummun 70 asteen lämpötilassa. Tämän ohjelman jälkeen sinulla on aina raikas ja puhdas rumpu valmiina uuteen pesuun. linebreak \r\n\r\nEco Bubble -pesutekniikka linebreak \r\n\r\nEco Bubble -pesutekniikka, jonka avulla varmistat tehokkaan pesutuloksen. Pesukone sekoittaa pesuaineen nopeasti veteen ja lisää samalla myös ilmaa pesuveteen, muodostaen pesuvaahtoa. Pesuvaahto imeytyy tekstiileihin nopeammin, jopa alhaisissakin lämpötiloissa, varmistaen parhaan mahdollisen pesutuloksen. linebreak \r\n\r\nBubble Soak™ linebreak \r\n\r\nEsipesuohjelma ilman mekaanista kitkaa. Vaikeimatkin tahrat voidaan liottaa tehokkaasti pois kuiduista 30min esiohjelman avulla. Tämän ohjelman asiosta saat parhaimman tahranpoisto tuloksen. Pesuainevaahdossa liottamalla tahrat lähtevät tehokkaammin pois. Voidaan käyttää seuraavien ohjelmien kanssa: Puuvilla, Synteettiset, Denim ja Baby Care. linebreak \r\n\r\nExtrahuuhtelut linebreak \r\n\r\nSamsung pesukoneessa on valittavana automaattipesuohjelmien yhteydessä 1-5 huuhtelukertaa, oman valinnan mukaan (pikapesuohjelmat eivät kuulu joukkoon) . Huuhtelu jaksoja voidaan nostaa tai laskea tilanteen vaatimalla tavalla.', 2, 'https://www.power.fi/images/products/8806088679327_Samsung_WD80M4433IWEE_EN_F.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/8806088679327_Samsung_WD80M4433IWEE_EN_2.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/8806088679327_Samsung_WD80M4433IWEE_EN_F.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'Valmistaja: Samsung,\r\nSyöttötapa: Edestä täytettävä,\r\nPyykinpesukoneen tyyppi: Edestä täytettävä pyykinpesukone,\r\nSisävalaistus: Ei,\r\nOven saranat: Vasen,\r\nPäävärit: Valkoinen,\r\nOven kätisyys vaihdettavissa: Ei,\r\nLinkousvaihtoehdot: Kyllä'),
(14, 'Samsung Galaxy A3 Black', '298.90', 'Uusissa Galaxy A -sarjan puhelimissa on parempi kamera, terävämpi näyttö ja pidempi akkukesto. Voit käyttää puhelinta huoletta rannalla, kaatosateessa tai laskettelurinteessä. Galaxy A on sekä pölyn- että vedenkestävä IP68-luokituksen* mukaisesti.\r\n \r\nMateriaalit ovat yhtä korkealaatuista lasia ja metallia kuin Galaxy S7 -mallissa, joten puhelin luo ylellisen tuntuman. Saat kohtuuhintaisen puhelimen, joka tuntuu yhtä ylelliseltä kuin huippumallit. Valittavana on kaksi kokoa ja kolme väriä.\r\n\r\n4,7” sAMOLED näyttö\r\n13 MP takakamera\r\n8 MP etukamera\r\n16 GB tallennustila\r\n8-ydinsuoritin 1,6GHz\r\nIP68 -sertifioitu\r\nSormenjälkitunnistin\r\nAndroid 6.0 ', 1, 'https://www.power.fi/images/products/8806088632520_SAMSUNG_GXYA3P_3.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/8806088632520_SAMSUNG_GXYA3P_F.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'https://www.power.fi/images/products/8806088632520_SAMSUNG_GXYA3P_2.jpg?height=450&width=450&bgcolor=fff&format=jpg', 'Valmistaja: Samsung,\r\nVirta ja akku: Akun kapasiteetti: 3000 mAh,\r\nVirta ja akku: Integroitu langaton lataus: Kyllä,\r\nIskunkestävä: Ei,\r\nKamera: CMOS 16,0 MP,\r\nVettä hylkivä 	Kyllä,\r\nKäyttöjärjestelmä (perhe): Android,\r\nKäyttöjärjestelmä (versio): Android 7.0,\r\nPaneelin tyyppi: LED,\r\nNäytön koko: 5,2",\r\nNäytön teknologia: LED'),
(16, 'Samsung BRB2600', '0.00', 'Samsungin A++ energialuokan integroitava jääpakastin invertteri kompressorilla.', 2, 'https://www.power.fi/images/products/8806088593142_Samsung_BRB260034WWEF_EN_8.jpg', 'https://www.power.fi/images/products/8806088593142_Samsung_BRB260034WWEF_EN_9.jpg?bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/8806088593142_Samsung_BRB260034WWEF_EN_F.jpg?bgcolor=fff&format=jpg&quality=80', 'Valmistaja: Samsung,\r\nIntegrointitapa: Täysin integroitu,\r\nSisävalaistus: Kyllä (LED),\r\nOven saranat: Oikea,\r\nPakastusluokka: 4 tähteä (****),\r\nTuorehylly: Ei,\r\nAutomaattisulatus: Kyllä'),
(17, 'Samsung RB29FS1', '428.00', 'Maksimoi säilytystila kätevästi linebreak \r\n\r\nJogurttipurkin kurkottelu jääkaapin takaosasta tarkoittaa usein selviytymistä elintarvikkeiden esteradasta. Yhdestä väärästä liikkeestä saattaa seurata litistynyt kuorrutus tai lasinsiruja lattialla. Helposti liukuva hylly on sijoitettu liikkuville kiskoille. Voit vetää hylly ulos, joten elintarvikkeiden järjestäminen on tehokasta ja ne on helppo ottaa käyttöön. Lisäksi näet nopeasti, mitä kaapin takaosassa on. linebreak \r\n\r\nLaatikko voidaan vetää kokonaan ulos linebreak \r\n\r\nPerinteisen pakastimen laatikoita pidemmälle ulosvedettävä laatikko helpottaa tuotteiden järjestelyä ja käyttöä, mukaan lukien suurikokoiset tuotteet, kuten 10 litran jäätelöpakkaus. Laatikko käyttää tilan optimaalisesti, sillä se voidaan vetää kokonaan ulos. Laatikon muotoilun ansiosta voit vetää laatikon ulos, vaikka jääkaapin ovi on auki vain 90 astetta. linebreak \r\n\r\nJäähdytä enemmän virvokkeita ja sammuta jano paremmin linebreak \r\n\r\nIso ovilokero on tavallisten jääkaappien lokeroita syvempi, joten se on kätevä janoisille talouksille. Nyt voit säilyttää suuret maito- ja mehupurkit ovessa ja myös virvokejuomatölkeille ja -pulloille on kaksi lokeroa. Pidätkö juhlat? Jäähdytä korkeat vesi- ja virvoitusjuomapullot sekä muut juomat isossa lokerossa. linebreak \r\n\r\nDigitaalinen invertterikompressori linebreak \r\n\r\nToisin kuin perinteiset kompressorit, joilla on vain kaksi käyttömallia (päällä ja pois päältä), Samsungin digitaalinen invertterikompressori tekee 5 erilaista liikesykliä kosteustasojen ja käyttötapojen perusteella. Tämä auttaa pitämään lämpötilan tasaisempana, vähentää kompressorin kulumista, lisää kestävyyttä ja tekee käyntiäänestä hiljaisen. linebreak ', 2, 'https://www.power.fi/images/products/8806085341241_F.jpg?bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/8806085341241_rb29fsrndww_web_f.png?bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/8806085341241_F.jpg?bgcolor=fff&format=jpg&quality=80', 'Valmistaja: Samsung,\r\nIntegroitava: Ei,\r\nIntegrointitapa: Ei integroitava,\r\nOven saranat: Oikea,\r\nSisävalaistus: Kyllä (LED),\r\nValaistuksen tyyppi: LED,\r\nPäävärit: Valkoinen,\r\nOven kätisyys vaihdettavissa: Kyllä'),
(18, 'Samsung LED-lamppu GU10', '9.90', 'Samsung 3.3W GU10 40D Lämmin valkoinen linebreak\r\nSäästää energiaa ja sitä kautta myös kustannuksiasi. LED-valaistusta voidaan käyttää korostamaan avaruutta.', 5, 'https://www.power.fi/images/products/8806086251952_GM8WH3003BD0.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://astro-chologist.com/wp-content/uploads/2013/08/blank-white.jpg', 'https://astro-chologist.com/wp-content/uploads/2013/08/blank-white.jpg', 'Valmistaja: Samsung,\r\nPistorasia: Gu10,\r\nLampun tyyppi: LED,\r\nTeho: 3,3 W,\r\nEAN: 8806086251952,\r\nTuotenumero: 53662'),
(19, 'Samsung Led-lamppu GA8WH50', '9.80', 'Energiaa säästävä E14 Light linebreak\r\nSäästää energiaa ja sitä kautta myös kustannuksiasi. Himmennettävä. Elegantti ja koristeellinen vaikutelma.', 5, 'https://www.power.fi/images/products/8806086268127_GA8WH5006CD0.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://astro-chologist.com/wp-content/uploads/2013/08/blank-white.jpg', 'https://astro-chologist.com/wp-content/uploads/2013/08/blank-white.jpg', 'Valmistaja: Samsung,\r\nPistorasia: E14,\r\nLampun tyyppi: LED,\r\nTeho: 5,8 W,\r\nEAN: 8806086251952,\r\nTuotenumero: 53669'),
(20, 'Samsung LED-lamppu GU1060', '9.80', 'Samsungin 5.7W Kynttilälamppu linebreak\r\nHuurrettu lämmin valkoinen E14 Himmentyvä. Elegantti ja koristeellinen valo. Energiatehokas. ', 5, 'https://www.power.fi/images/products/8806086273114_GA8WH5006AH0.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://astro-chologist.com/wp-content/uploads/2013/08/blank-white.jpg', 'https://astro-chologist.com/wp-content/uploads/2013/08/blank-white.jpg', 'Valmistaja: Samsung,\r\nPistorasia: E14,\r\nLampun tyyppi: LED,\r\nTeho: 5,8 W,\r\nEAN: 8806086251952,\r\nTuotenumero: 53669,\r\nEnergialuokka: A+'),
(21, 'Dyson DC33C PRO', '299.90', 'Dyson DC33C PRO linebreak \r\nTehokas pölypussiton imuri sykloniteknologialla linebreak\r\nRadial Root Cyclone™ -teknologia linebreak \r\nUudenlainen ilmavirtaus kerää entistä enemmän likaa ja mikroskooppista pölyä säiliöön. linebreak \r\nKaksitoiminen lattiasuulake linebreak \r\nSäädettävä lattiasuulake poistaa pölyn ja lian kaikilta lattiapinnoilta. linebreak \r\nBall™-teknologia linebreak \r\nKääntyy paikallaan ja kulkee perässä ketterästi ja vaivattomasti ympäri kotia. linebreak \r\nHygieeninen pölysäiliön tyhjennys linebreak \r\nHelppo tyhjennys painiketta painamalla.', 4, 'https://www.power.fi/images/products/5025155024195_Dyson_602628_EN_F.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/5025155024195_Dyson_602628_EN_2.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/5025155024195_Dyson_602628_EN_F.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'Valmistaja: Dyson,\r\nToimintasäde: 10 Metriä,\r\nAutomaattinen johdon kelaus: Kyllä,\r\nPussiton: Kyllä,\r\nJohdon pituus: 6,5 Metriä,\r\nToiset värit: Keltainen,\r\nJohdon kelaus: Kyllä,\r\nPölykapasiteetti: 2 Litra'),
(22, 'Dyson Bigball Parquet', '449.90', 'Ainoa imuri, jota ei tarvitse huoltaa ja jonka imuteho ei heikkene. linebreak\r\n\r\nEi pestäviä tai vaihdettavia suodattimia. Ei erikseen ostettavia pölypusseja. Se sieppaa mikroskooppisen pölyn, joka tukkii kaikki muut imurit - eikä sinun tarvitse pestä tai vaihtaa likaisia suodattimia. linebreak\r\n\r\nPerinteiset perässä vedettävät imurit voivat kaatua kumoon, jolloin siivoaminen keskeytyy. linebreak\r\nUusi hygieninen säiliön tyhjennysmekanismi helpottaa säiliöön kerääntyneen pölyn ja lian poistamista. linebreak\r\n\r\nHiilikuidusta tehty turbiinisuulake poistaa lian matoista ja hienojakoisen pölyn kovilta lattiapinnoilta.\r\nBall-teknologia helpottaa esteiden kiertämistä. Laitteen virtaviivainen runko estää laitetta jäämästä mihinkään kiinni.', 4, 'https://www.power.fi/images/products/5025155024539_DYSON_BIGBALLPARQUET_F.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/5025155024539_DYSON_BIGBALLPARQUET_2.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/5025155024539_DYSON_BIGBALLPARQUET_3.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'Valmistaja: Dyson,\r\nToimintasäde: 10,75 Metriä,\r\nAutomaattinen johdon kelaus: Kyllä,\r\nPäävärit: Harmaa,\r\nPussiton: Kyllä,\r\nJohdon pituus: 6,6 Metri,\r\nToiset värit: Punainen,\r\nJohdon kelaus: Kyllä,\r\nMoottoriteho: 800 W'),
(23, 'Samsung J5', '199.00', 'Enemmän valoa kuviin linebreak \r\n\r\nGalaxy J5 sopii kaikille, jotka haluavat viettää aikaa yhdessä perheen ja ystävien kanssa. Siinä on suuri, veitsenterävä näyttö ja todella hyvä kamera, joka päästää enemmän valoa sisään ja tarjoaa monia käteviä selfie-toimintoja. Siten se sopii kaikkiin yhteisiin seikkailuihin. Elämä on valoisampaa Galaxy J5:n avulla. linebreak \r\n\r\nSuuri HD-tarkkuuksinen Super AMOLED -näyttö linebreak \r\n\r\n5,2 tuuman Super AMOLED -näytön HD-tarkkuus, terävä kontrasti ja todella hyvät värit tekevät kuvasta miellyttävän. Tehokas suorituskykyGalaxy J:ssä on 1,2 GHz Quad Core -prosessori, jolla voit katsella videoita, selata verkkosivuja ja pelata ongelmitta. linebreak \r\n\r\nNopeasti aktivoituva kamera linebreak \r\n\r\nErinomainen nopeasti aktivoituva 13 Mp:n kamera, jossa on valoherkkä objektiivi, mahdollistaa selkeät ja hienot kuvat myös heikommassa valaistuksessa. Lisäksi hauskoja selfie-toimintoja ja salama toimii sekä etu- että takakameran kanssa. linebreak ', 1, 'https://www.power.fi/images/products/8806088390802_SM-J510FN_F.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/8806088390802_SM-J510FN_2.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'https://www.power.fi/images/products/8806088390802_SM-J510FN_4.jpg?height=450&width=450&bgcolor=fff&format=jpg&quality=80', 'Valmistaja: Samsung,\r\nFyysinen näppäimistö: Ei,\r\nTaitettava muotoilu: Ei,\r\nPäävärit: Musta,\r\nSenioreille sopiva: Ei,\r\nLiukuva muotoilu: Ei,\r\nAkun kapasiteetti: 3100 mAh,\r\nIntegroitu langaton lataus: Ei,\r\nLatausjohdon tyyppi: Micro-USB,\r\nVaihdettava akku: Kyllä'),
(24, 'Adidas voimapyörä ', '19.90', 'Adidas voimapyörä  linebreak\r\nTehokas ja kestävä kuntopyörä niin vasta-alkajille kuin pitempään harrastaneille', 3, 'https://www.prisma.fi/wcsstore/PRISMA_StorefrontAssetStore/images/product/iso/0885652002370_kuva1.jpg', 'https://www.prisma.fi/wcsstore/PRISMA_StorefrontAssetStore/images/product/iso/0885652002370_kuva2.jpg', 'https://www.prisma.fi/wcsstore/PRISMA_StorefrontAssetStore/images/product/iso/0885652002370_kuva3.jpg', 'Takuu: 1 vuosi'),
(25, 'Adidas jumppapallo 65 cm', '25.50', 'Adidas jumppapallo linebreak\r\nPerinteinen jumppapallo', 3, 'https://www.prisma.fi/wcsstore/PRISMA_StorefrontAssetStore/images/product/iso/0885652005340_kuva1.jpg', 'https://www.prisma.fi/wcsstore/PRISMA_StorefrontAssetStore/images/product/iso/0885652005340_kuva3.jpg', 'https://www.prisma.fi/wcsstore/PRISMA_StorefrontAssetStore/images/product/iso/0885652005340_kuva2.jpg', 'Koko: 65 cm');

-- --------------------------------------------------------

--
-- Rakenne taululle `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `score` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7EEF84F04584665A` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Vedos taulusta `review`
--

INSERT INTO `review` (`id`, `content`, `user`, `score`, `product_id`) VALUES
(1, 'Android-puhelinten ykkönen tällä hetkellä!', 'procer221', 'Kiitettävä', 2),
(2, 'Toimii ja hyvin!', 'tester4', 'Hyvä', 1),
(4, 'Toimiva Samsung kalliimmassa hintaluokassaan.', 'techguy', 'Kiitettävä', 2),
(5, 'Näyttö on vaikuttava mutta muuten ylihintainen', 'specialisti', 'Hyvä', 2),
(6, 'Hyvä. Ei kiitettävä', 'antton', 'Hyvä', 2),
(7, 'Sain viallisen kappaleen.', 'marko34', 'Huono', 2);

--
-- Rajoitteet vedostauluille
--

--
-- Rajoitteet taululle `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Rajoitteet taululle `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `FK_7EEF84F04584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
