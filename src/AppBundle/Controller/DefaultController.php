<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\Query\ResultSetMapping;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

      $products = $this->getDoctrine()
                ->getRepository('AppBundle:Product')
                ->findAll();

      $categories = $this->getDoctrine()
                ->getRepository('AppBundle:Category')
                ->findAll();

      $firstItem = array_shift($products);
            if (null !== $firstItem) {
                $firstItem->index = 1;
                if (strlen($firstItem->getDescription()) > 210) {
                    $string = substr($firstItem->getDescription(), 0, 210).'...';
                    $firstItem->descriptionShortened = $string;
                } else {
                  $firstItem->descriptionShortened = $firstItem->getDescription();
                }
            }

      foreach (array_values($products) as $i => $product) {
        $product->index = $i+1;
        if (strlen($product->getDescription()) > 210) {
            $string = substr($product->getDescription(), 0, 210).'...';
            $product->descriptionShortened = $string;
          } else {
            $product->descriptionShortened = $product->getDescription();
          }
      }

      foreach ($products as $product) {
          if ($product->getCategory()->getId() !== 1) {
            $objectarray[]=$product;
          }
      }

      $products = array_slice($products, 0, 7);

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'firstItem' => $firstItem,
            'products'=> $products,
            'categories'=> $categories
        ]);
    }

    /**
     * @Route("/elektroniikka", name="elektroniikka")
     */
    public function elektroniikkaAction(Request $request)
    {

      $products = $this->getDoctrine()
                ->getRepository('AppBundle:Product')->createQueryBuilder('u')
                ->where('u.category = 1')
                ->getQuery()
                ->getArrayResult();

      $em = $this->getDoctrine()->getManager();

      $dql   = "SELECT a FROM AppBundle:Product a WHERE a.category = 1";
      $query = $em->createQuery($dql);

      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
          $query, /* query NOT result */
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
      );

      // parameters to template
      return $this->render('default/elektroniikka.html.twig', array(
        'products'=> $products,
        'pagination' => $pagination
      ));
    }

    /**
     * @Route("/kodinkoneet", name="kodinkoneet")
     */
    public function kodinkoneetAction(Request $request)
    {

      $products = $this->getDoctrine()
                ->getRepository('AppBundle:Product')->createQueryBuilder('u')
                ->where('u.category = 2')
                ->getQuery()
                ->getArrayResult();

      $em = $this->getDoctrine()->getManager();

      $dql   = "SELECT a FROM AppBundle:Product a WHERE a.category = 2";
      $query = $em->createQuery($dql);

      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
          $query, /* query NOT result */
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
      );

      // parameters to template
      return $this->render('default/kodinkoneet.html.twig', array(
        'products'=> $products,
        'pagination' => $pagination
      ));
    }
    /**
     * @Route("/urheilu", name="urheilu")
     */
    public function urheiluAction(Request $request)
    {

      $products = $this->getDoctrine()
                ->getRepository('AppBundle:Product')->createQueryBuilder('u')
                ->where('u.category = 3')
                ->getQuery()
                ->getArrayResult();

      $em = $this->getDoctrine()->getManager();

      $dql   = "SELECT a FROM AppBundle:Product a WHERE a.category = 3";
      $query = $em->createQuery($dql);

      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
          $query, /* query NOT result */
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
      );

      // parameters to template
      return $this->render('default/urheilu.html.twig', array(
        'products'=> $products,
        'pagination' => $pagination
      ));
    }

    /**
     * @Route("/koti", name="koti")
     */
    public function kotiAction(Request $request)
    {

      $products = $this->getDoctrine()
                ->getRepository('AppBundle:Product')->createQueryBuilder('u')
                ->where('u.category = 4')
                ->getQuery()
                ->getArrayResult();

      $em = $this->getDoctrine()->getManager();

      $dql   = "SELECT a FROM AppBundle:Product a WHERE a.category = 4";
      $query = $em->createQuery($dql);

      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
          $query, /* query NOT result */
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
      );

      // parameters to template
      return $this->render('default/koti.html.twig', array(
        'products'=> $products,
        'pagination' => $pagination
      ));
    }

    /**
     * @Route("/remontointi", name="remontointi")
     */
    public function remontointiAction(Request $request)
    {

      $products = $this->getDoctrine()
                ->getRepository('AppBundle:Product')->createQueryBuilder('u')
                ->where('u.category = 5')
                ->getQuery()
                ->getArrayResult();

      $em = $this->getDoctrine()->getManager();

      $dql   = "SELECT a FROM AppBundle:Product a WHERE a.category = 5";
      $query = $em->createQuery($dql);

      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
          $query, /* query NOT result */
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
      );

      // parameters to template
      return $this->render('default/remontointi.html.twig', array(
        'products'=> $products,
        'pagination' => $pagination
      ));
    }

    /**
     * @Route("/createproduct/{id}", name="createproduct")
     */
     public function createAction($id, Request $request)
     {
       $category = $this->getDoctrine()
                 ->getRepository('AppBundle:Category')
                 ->find($id);

         $product = new Product();
         $product->setName('Bike safe helmet');
         $product->setPrice(80.99);
         $product->setDescription('Safe and stylish!');

         // relate this product to the category
        $product->setCategory($category);

        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->persist($product);
        $em->flush();

        return new Response(
            'Saved new product with id: '.$product->getName()
            .' and new category with id: '.$category->getName()
        );
     }

     /**
      * @Route("/showproduct/{id}", name="showproduct")
      */
      public function showAction($id)
      {
        $product = $this->getDoctrine()
                  ->getRepository('AppBundle:Product')
                  ->find($id);

        $categoryName = $product->getCategory()->getName();
        return new Response('Name is: '.$product->getName().', price is: $'.$product->getPrice().', category is: '.$categoryName);
      }

      /**
       * @Route("/search/{category}/{id}", name="search")
       */
       public function searchAction($category, $id)
       {
         /*
        SELECT category_id FROM product LEFT JOIN category ON product.category_id=category.id WHERE product.name LIKE 'sam%'
                   */
      $rsm = new ResultSetMapping();
      $em = $this->getDoctrine()->getManager();
      $result = $em->getRepository("AppBundle:Product")->createQueryBuilder('u')
            ->join('u.category', 'c')
            ->where('u.name LIKE :name')
            ->setParameter('name', '%'.$id.'%')
            ->getQuery()
            ->getArrayResult();

      if ($category == 0) { // faster search from every category
        $categoryAmount = array(
          "category1Amount" => 0, "category2Amount" => 0, "category3Amount" => 0, "category4Amount" => 0, "category5Amount" => 0
        );
        if ($result) {
          foreach ($result as $i => $product) {
            $sql = "SELECT category_id FROM product LEFT JOIN category ON product.category_id=category.id WHERE product.id = '".$result[$i]['id']."'";

            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $categoryResult = $stmt->fetch();

            if ($categoryResult['category_id'] == 1 && $categoryAmount['category1Amount'] <= 2) { //we want the 3 first, but arrays start at 0
              $result[$i]['category'] = $categoryResult['category_id'];
              $categoryAmount['category1Amount'] = $categoryAmount['category1Amount'] + 1;
            }
            if ($categoryResult['category_id'] == 2 && $categoryAmount['category2Amount'] <= 2) {
              $result[$i]['category'] = $categoryResult['category_id'];
              $categoryAmount['category2Amount'] = $categoryAmount['category2Amount'] + 1;
            }
            if ($categoryResult['category_id'] == 3 && $categoryAmount['category3Amount'] <= 2) {
              $result[$i]['category'] = $categoryResult['category_id'];
              $categoryAmount['category3Amount'] = $categoryAmount['category3Amount'] + 1;
            }
            if ($categoryResult['category_id'] == 4 && $categoryAmount['category4Amount'] <= 2) {
              $result[$i]['category'] = $categoryResult['category_id'];
              $categoryAmount['category4Amount'] = $categoryAmount['category4Amount'] + 1;
            }
            if ($categoryResult['category_id'] == 5 && $categoryAmount['category5Amount'] <= 2) {
              $result[$i]['category'] = $categoryResult['category_id'];
              $categoryAmount['category5Amount'] = $categoryAmount['category5Amount'] + 1;
            }

          }
        }
      } else { // search page search
        $result1 = array();
        foreach ($result as $i => $product) {
          $sql = "SELECT category_id FROM product LEFT JOIN category ON product.category_id=category.id WHERE product.id = '".$result[$i]['id']."'";

          $stmt = $em->getConnection()->prepare($sql);
          $stmt->execute();
          $categoryResult = $stmt->fetch();
          if ($category == $categoryResult['category_id']) {
              $result1[] = $result[$i];
          }
        }
        $result = $result1;
      }

      //var_dump($categoryAmount);
      //return new Response('Name is: ');
      return new JsonResponse(array('result' => $result));

       }

       /**
        * @Route("/searchpage/{id}/{id2}", name="searchpage")
        */
        public function searchPageAction($id)
        {
          $product = $this->getDoctrine()
                    ->getRepository('AppBundle:Product')
                    ->find(1);

          return $this->render('default/searchpage.html.twig', [
              'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
              'product'=> $product
          ]);
        }
}
