$(document).ready(function(){
  var query = window.location.toString();

  var parts = query.split('/searchpage/', 2);
  var params = parts[1].split('/', 2);
  $("input[name=radioName][value="+params[0]+"]").attr('checked', true);
  if (params[0] < 1 || params[0] > 6) {
    $('input[name=radioName][value=1]').attr('checked', true);
  }
  $('#filter').val(params[1]);

  $("#searchButton").click(function() {
      var option = $('input[name=radioName]:checked', '#myForm').val();
      var word = $('#filter').val();
      getRequest();
   }
  );

  if(params[0] != 0){
      getRequest();
  }

  function getRequest() {
        var word = $('#filter').val();
        var option = $('input[name=radioName]:checked', '#myForm').val();
        var url = "http://localhost:8000/search/" + option + "/" + word;
        var xhr = new XMLHttpRequest();

        if (word !== '' && word.length > 3) {
          xhr.open('POST', url, true);
          xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
          xhr.addEventListener('load', function() {
            $("#category1").empty();

            var responseObject = JSON.parse(this.response);
            console.log(responseObject.result);

            var items = responseObject.result;

            for ( var i = 0; i < items.length; i++) {
              var item = items[i];
              item.description = item.description.substring(0, 200) + '...';

              var newDiv = "<a href='/product/details/"+item.id+"'><img src="+item.imgurl1+" style='padding: 16px; max-height: 100px; max-width: 90px; float: left;'></a>'";
              $(newDiv).appendTo("#category1");

              newDiv = "<div class='span-4  tmp'><p>" + item.name + "</p></div>";
              $(newDiv).appendTo("#category1");

              newDiv = "<div class='span-6 tmp'><p>" + item.description + "</p></div>";
              $(newDiv).appendTo("#category1");

              newDiv = "<div class='span-4 append-10 last tmp'><p>€" + item.price + "<a style='padding-left: 10px;' href='/product/details/"+item.id+"'>Lue lisää<p></a></div>";
              $(newDiv).appendTo("#category1");

            }

              //$( "#search" ).append(this.response);
              //$('#search').css('width: 500px; height: 100px; border: 1px solid red; overflow: hidden; overflow-y : scroll;'); //set max height
            if (this.response.length == 0) {
              $("#category1").empty();
            } else {
              console.log("No token received");
            }
          });

          xhr.send();
        } if (word.length < 1) {
          console.log('no word!');
          $("#category1").empty();
          //$("#search").css('height: 100px;'); //set max height
        }
  }

  $("#filter").keyup(function(){

  });

});
