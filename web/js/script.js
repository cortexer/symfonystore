$(document).ready(function(){

  $("#filter").keyup(function(){

  var word = $('#filter').val();
  var url = "http://localhost:8000/search/" + 0 + "/" + word;
  var xhr = new XMLHttpRequest();

  if (word !== '' && word.length > 3) {
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xhr.addEventListener('load', function() {
      $("#category1").empty();
      $("#category2").empty();
      $("#category3").empty();
      $("#category4").empty();
      $("#category5").empty();

      var responseObject = JSON.parse(this.response);
      console.log(responseObject.result);

      var items = responseObject.result;

      for ( var i = 0; i < items.length; i++) {
        var item = items[i];
        item.description = item.description.substring(0, 200) + '...';

        if (!elektroniikkaHeader && item.category == 1) {
          var elektroniikkaHeader = $("<a style='padding-top: 5px; padding-left: 10px; padding-right: 12px; float:right;' href='/searchpage/"+item.category+"/"+word+"'>Näytä lisää tuloksia</a><b style='float: right; padding: 5px;'>Elektroniikka</b>").appendTo("#category1");
        }
        if (!kodinkoneetHeader && item.category == 2) {
          var kodinkoneetHeader = $("<a style='padding-top: 5px; padding-left: 10px; padding-right: 12px; float:right;' href='/searchpage/"+item.category+"/"+word+"'>Näytä lisää tuloksia</a><b style='float: right; padding: 5px;'>Kodinkoneet</b>").appendTo("#category2");
        }
        if (!urheiluHeader && item.category == 3) {
          var urheiluHeader = $("<a style='padding-top: 5px; padding-left: 10px; padding-right: 12px; float:right;' href='/searchpage/"+item.category+"/"+word+"'>Näytä lisää tuloksia</a><b style='float: right; padding: 5px;'>Urheilu</b>").appendTo("#category3");
        }
        if (!kotiHeader && item.category == 4) {
          var kotiHeader = $("<a style='padding-top: 5px; padding-left: 10px; padding-right: 12px; float:right;' href='/searchpage/"+item.category+"/"+word+"'>Näytä lisää tuloksia</a><b style='float: right; padding: 5px;'>Koti</b>").appendTo("#category4");
        }
        if (!remontointiHeader && item.category == 5) {
          var remontointiHeader = $("<a style='padding-top: 5px; padding-left: 10px; padding-right: 12px; float:right;' href='/searchpage/"+item.category+"/"+word+"'>Näytä lisää tuloksia</a><b style='float: right; padding: 5px;'>Remontointi</b>").appendTo("#category5");
        }

        var newDiv = "<a href='/product/details/"+item.id+"'><img src="+item.imgurl1+" style='padding: 16px; max-height: 100px; max-width: 90px; float: left;'></a>'";
        $(newDiv).appendTo("#category"+item.category+"");

        newDiv = "<div class='span-4  tmp'><p>" + item.name + "</p></div>";
        $(newDiv).appendTo("#category"+item.category+"");

        newDiv = "<div class='span-6 tmp'><p>" + item.description + "</p></div>";
        $(newDiv).appendTo("#category"+item.category+"");

        newDiv = "<div class='span-4 append-10 last tmp'><p>£" + item.price + "</p></div>";
        $(newDiv).appendTo("#category"+item.category+"");

      }

        //$( "#search" ).append(this.response);
        //$('#search').css('width: 500px; height: 100px; border: 1px solid red; overflow: hidden; overflow-y : scroll;'); //set max height
      if (this.response.length == 0) {
        $("#category1").empty();
        $("#category2").empty();
        $("#category3").empty();
        $("#category4").empty();
        $("#category5").empty();
      } else {
        console.log("No token received");
      }
    });

    xhr.send();
  } if (word.length < 1) {
    console.log('no word!');
    $("#category1").empty();
    $("#category2").empty();
    $("#category3").empty();
    $("#category4").empty();
    $("#category5").empty();
    //$("#search").css('height: 100px;'); //set max height
  }
  });

  $("#searchButton").click(function() {
     var searchWord = $('#filter').val();
     window.location.href = "/searchpage/0/" + searchWord;
     //alert('button clicked');
   }
);
});
