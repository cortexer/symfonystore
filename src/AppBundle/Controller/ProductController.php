<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProductController extends Controller
{
    /**
     * @Route("/products", name="product_list")
     */
    public function listAction(Request $request) {
        $products = $this->getDoctrine()
                  ->getRepository('AppBundle:Product')
                  ->findAll();

        $em = $this->getDoctrine()->getManager();

    $dql   = "SELECT a FROM AppBundle:Product a";
    $query = $em->createQuery($dql);

    $paginator  = $this->get('knp_paginator');
    $pagination = $paginator->paginate(
        $query, /* query NOT result */
        $request->query->getInt('page', 1)/*page number*/,
        5/*limit per page*/
    );

    // parameters to template
    return $this->render('product/index.html.twig', array(
      'products'=> $products,
      'pagination' => $pagination
    ));
    }

    /**
     * @Route("/product/edit/{id}", name="product_edit")
     */
/*    public function editAction($id, Request $request) {
      $product = $this->getDoctrine()
                ->getRepository('AppBundle:Product')
                ->find($id);

      $now = new\DateTime('now');

      $product->{"categoryName"} = $product->getCategory()->getName();

      $form = $this->createFormBuilder($product)
          ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style'=> 'margin-bottom:15px')))
          ->add('categoryName', ChoiceType::class, array('choices' => array('Low' => 1, 'Normal' => 2, 'High' => 3), 'attr' => array('class' => 'form-control', 'style'=> 'margin-bottom:15px')))
          ->add('save', SubmitType::class, array('label' => 'Update Product', 'attr' => array('class' => 'btn btn primary', 'style'=> 'margin-bottom:15px')))
          ->getForm();

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid()) {
            $name = $form['name']->getData();
            //$categoryName = $form['categoryName']->getData();

            $now = new\DateTime('now');

            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('AppBundle:Product')->find($id);

            $categoryId = $form['categoryName']->getData();
            $categoryName = $em->getRepository('AppBundle:Category')->find($categoryId);

            $product->setName($name);
            $product->setCategory($categoryName);

            $em->flush();

            $this->addFlash(
              'notice',
              'Product Updated!'
            );

            return $this->redirectToRoute('product_list');
      }

      return $this->render('product/edit.html.twig', array(
          'product'=> $product,
          'form' => $form->createView()
      ));
    }
*/
    /**
     * @Route("/product/details/{id}", name="product_details")
     */
    public function detailsAction($id) {
      $product = $this->getDoctrine()
                ->getRepository('AppBundle:Product')
                ->find($id);

      $reviews = $this->getDoctrine()
                ->getRepository('AppBundle:Review');

      //$reviews = $reviews->getReviews()->findOneBy(2);

      $categoryName = $product->getCategory()->getName();

      $arrayDescription = explode(' linebreak ', $product->getDescription());
      $product->descriptionList = $arrayDescription;

      $myString = $product->getDetails();
      $myArray = explode(',', $myString);
      $product->detailsList = $myArray;

      return $this->render('product/details.html.twig', array(
          'product'=> $product,
          'category'=> $categoryName,
      ));
    }

    /**
     * @Route("/product/delete/{id}", name="product_delete")
     */
/*    public function deleteAction($id) {
      $em = $this->getDoctrine()->getManager();
      $product = $em->getRepository('AppBundle:Product')->find($id);

      $em->remove($product);
      $em->flush();

      $this->addFlash(
        'notice',
        'Product Removed!'
      );

      return $this->redirectToRoute('product_list');
    }*/
}
